<?php

/**
 * Une API fournissant tous les outils pour gérer les imports dans les calendriers
 *
 * fonction à utiliser :
 *     ```
 *     $requeter = charger_fonction('requeter_calendrier','inc');
 *     $requeter($url_base, $dataset, $filtres, $options, $taille_max);
 *     ```
 * @link https://programmer.spip.net/charger_fonction
 * @plugin     Erdv_dashboard
 * @copyright  2024
 * @author     Vincent CALLIES
 * @licence    GNU
 * @package    SPIP\Erdv_dashboard\inc
 */

/**
 * Test de sécurité pour vérifier que SPIP est chargé 
 * et donc que les fichiers ne sont pas appelés en dehors de l'usage de SPIP
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Renvoie, à partir de l'url du service, le tableau des données demandées.
 * Le service utilise dans ce cas une chaine JSON qui est décodée pour fournir
 * le tableau de sortie. Le flux retourné par le service est systématiquement
 * transcodé dans le charset du site avant d'être décodé.
 *
 * @credit Fonction inspirée du travail d'Eric et de son plugin Territoires
 * @uses recuperer_url()
 *
 * @param string      $url_base   Endpoint du serveur
 * @param null|string $dataset   Nom de la collection ou vide si on veut récupérer l'index des collections du serveur.
 * @param null|array  $filtres    Tableau des filtres à appliquer à la collection
 * @param null|array  $options    Tableau des options à appliquer à la requête
 * @param null|int    $taille_max Taille maximale du flux récupéré suite à la requête.
 *                                La valeur entière `0` désigne la taille par défaut.
 *
 * @throws JsonException
 *
 * @return array Tableau de la réponse.
 *               Si l'index `erreur['status']` indique le statut de la réponse.
 *               La valeur 200 permet de tester une requête réussie et dans ce cas
 *               l'index `page` du flux reçu est retourné après décodage JSON.
 */
function inc_requeter_calendrier_dist (string $url_base, ?string $dataset = '', ?array $filtres = [], ?array $options = [], ?int $taille_max = 0) : array {
	// Calcul de l'url complète
	// -- initialisation
	$url = $dataset
		? "{$url_base}/{$dataset}"
		: $url_base;
	// -- ajout des filtres si nécessaire
	//    l'absence de tout argument peut être la simple sollicitation d'une description
	if (!empty($filtres)){
		if (isset($filtres['limit']) and is_int($filtres['limit'])){
			$url .= "/records?limit={$filtres['limit']}";
		} else {
			$url .= "/records?limit=10";
		}
		unset($filtres['limit']);
		// -- ajout des filtres
		foreach ($filtres as $_critere => $_valeur) {
			if (is_array($_valeur)){
				foreach ($_valeur as $__filtre) {
					$url .= '&' . urlencode($_critere) . '=' . urlencode($__filtre);
				}
			} else {
				$url .= '&' . urlencode($_critere) . '=' . urlencode($_valeur);
			}
		}
	}
	// -- les options
	include_spip('inc/distant'); // offre des constantes utiles
	$default = [
		'transcoder' => false,
		'methode' => 'GET',
		'taille_max' => null,
		'headers' => [],
		'datas' => '',
		'boundary' => '',
		'refuser_gz' => false,
		'if_modified_since' => '',
		'uri_referer' => '',
		'file' => '',
		'follow_location' => 10,
		'version_http' => _INC_DISTANT_VERSION_HTTP,
	];
	// Avec la fonction array_merge, si une clé existe dans les 2 tableaux, 
	// alors l'élément du premier sera utilisé
	// et la clé correspondante du second sera ignorée.
	$options = array_merge($default, $options);
	if (is_null($options['taille_max'])) {
		$options['taille_max'] = $options['file'] ? _COPIE_LOCALE_MAX_SIZE : _INC_DISTANT_MAX_SIZE;
	}

	// Acquisition du flux de données
	if ($url){
		$flux = recuperer_url($url, $options);
		$reponse = [];
		if (
			!$flux
			or ($flux['status'] === 404)
			or empty($flux['page'])
		) {
			// Erreur serveur 501 (Not Implemented)
			$reponse['erreur'] = [
				'status'  => 501,
				'type'    => 'serveur_api_indisponible',
				'element' => 'serveur',
				'valeur'  => $url_base,
				'extra'   => ['url' => $url]
			];
		} else {
			try {
				// Transformation de la chaîne json reçue en tableau associatif
				$reponse = json_decode($flux['page'], true, 512, JSON_THROW_ON_ERROR);
			} catch (Exception $erreur) {
				// Erreur serveur 520 (Unknown Error)
				$reponse['erreur'] = [
					'status'  => 520,
					'type'    => 'serveur_reponse_json',
					'element' => 'serveur',
					'valeur'  => $url,
					'extra'   => [
						'url'         => $url,
						'erreur_json' => $erreur->getMessage()
					]
				];
			}
		}
	}
	return $reponse;
}