<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = [

	'dashboard_erdv_desc' => 'Tableau de bord des rendez-vous & calendriers.',
	'dashboard_erdv_nom' => '<em>Des rendez-vous économes</em>',
	'groupe_archivage_anonymisation_nom' => 'Archivage et anonymisation',
	'info_1_anonyme' => '1 rendez-vous anonymisé',
	'info_1_archive' => '1 rendez-vous archivé',
	'info_aucun_anonyme' => 'Aucun rendez-vous anonymisé',
	'info_aucun_archive' => 'Aucun rendez-vous archivé',
	'info_nb_anonymes' => '@nb@ rendez-vous anonymisés',
	'info_nb_archives' => '@nb@ rendez-vous archivés',
	'type_controle_erdvs_anonyme_desc' => 'Visualisez les rendez-vous anonymisés. Il n’est pas possible de changer le statut autrement que pour une destruction car l’anonymisation est un statut définitif. Cette destruction modifiera vos statistiques.',
	'type_controle_erdvs_anonyme_nom' => 'Liste des rendez-vous anonymisés.',
	'type_controle_erdvs_archive_desc' => 'Visualisez les rendez-vous archivés. Changer, si nécessaire, leur statut à l’aide de la puce. Ce changement ne sera que temporaire, puisque le passage au statut d’archivé est automatisé pour les rendez-vous ayant dépassé une certaine période.',
	'type_controle_erdvs_archive_nom' => 'Liste des rendez-vous archivés.',
	'type_controle_erdvs_anonymisation_nom' => 'État de l’anonymisation des rendez-vous',
	'type_controle_erdvs_anonymisation_desc' => 'Vérification de la complétude de l’anonymisation des rendez-vous. Proposition de correction des anomalies si nécessaire.',
	'groupe_calendriers_nom' => 'Imports de calendriers',
	'type_controle_calendriers_vacances_nom' => 'Les calendriers des vacances scolaires',
	'type_controle_calendriers_vacances_desc' => 'Importer les dates des vacances scolaires par zone dans des calendriers.',
	'info_zone_a' => 'Zone A',
	'info_zone_b' => 'Zone B',
	'info_zone_c' => 'Zone C',
	'label_zones' => 'Choisissez une zone ou des zones dont vous souhaitez importer les dates',
	'label_ecalendriers' => 'Choisissez le calendrier ou les calendriers où les dates seront importées',
	'info_annee_scolaire_en_cours' => 'Annee scolaire en cours',
	'info_annee_scolaire_suivante' => 'Annee scolaire suivante',
	'info_rentree_des_enseignants' => 'Rentrée des enseignantes et des enseignants',
	'info_vacances_scolaires' => 'Vacances scolaires',
];
