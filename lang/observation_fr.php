<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = [
	'erdv_dashboard_erdvs_anonymisation_column_void' => 'Une colonne n’a pas vidée !',
	'erdv_dashboard_calendriers_vacances_log_abs_vac' => 'Vacances non indiquées : @annee@ - @zone@ - @type@',
];