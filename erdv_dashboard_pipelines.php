<?php
/**
 * Utilisations de pipelines par Erdv_dashboard
 *
 * @plugin     erdv_dashboard
 * @copyright  2024
 * @author     Vincent CALLIES
 * @licence    GNU
 * @package    SPIP\Erdv_dashboard\Pipelines
 * @link https://programmer.spip.net/-Liste-des-pipelines- Liste des pipelines de SPIP
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Utiliser ce pipeline permet d'ajouter du contenu sur certaines pages,
 * notamment des formulaires de liaisons entre objets
 *
 * Ce plugin :
 * - 
 * - 
 *
 * @pipeline affiche_milieu
 * @param  array $flux Données du pipeline
 * @return array       Données du pipeline
 */
function erdv_dashboard_affiche_milieu($flux) {
	$texte = '';

	if ($e = trouver_objet_exec($flux['args']['exec']) and $type = $e['type'] ){

		if ($texte) {
			if ($p = strpos($flux['data'], '<!--affiche_milieu-->')) {
				$flux['data'] = substr_replace($flux['data'], $texte, $p, 0);
			} else {
				$flux['data'] .= $texte;
			}
		}
	}

	return $flux;
}

/**
 * Pipeline appelé à l'issue d'une association ou d'une révocation d'association
 * d'un objet sur un autre objet.
 * 
 * @pipeline post_edition_lien
 * @param  array $flux Données du pipeline
 * @return array       Données du pipeline
 */
 function erdv_dashboard_post_edition_lien($flux){
	// l'action menée peut être modifier | delete | insert
	if (isset($flux['args']['action']) and $action = $flux['args']['action']){

		if (isset($flux['args']['objet_source']) and $objet_source = $flux['args']['objet_source']
			and isset($flux['args']['id_objet_source']) and $id_objet_source = intval($flux['args']['id_objet_source'])
			and isset($flux['args']['objet']) and $objet = $flux['args']['objet']
			and isset($flux['args']['id_objet']) and $id_objet = intval($flux['args']['id_objet'])
		){

			
		}
	}
	return $flux;
}
